from chess_vi.board import Board


b = Board(9)
for s in b.spaces:
    row = []
    for n in b.spaces:
        distance = 0
        if n in s.edge_neighbors().values():
            distance = 1
        elif n in s.corner_neighbors().values():
            distance = 1
        row.append(str(distance))
    print ', '.join(row)
