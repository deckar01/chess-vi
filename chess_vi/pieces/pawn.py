from piece import Piece


class Pawn(Piece):
    icon = u'\u2659'

    def moves(self):
        direction_index = self.space.corner_directions.index(self.player.side)
        # Only allow edge moves if there is not a piece in the way.
        edge_directions = [
            self.space.edge_directions[(direction_index + rotation) % 6]
            for rotation in [2, 3]
        ]
        short_edge_moves = {
            direction: self.space.get_edge(direction)
            for direction in edge_directions
        }
        short_edge_moves = {
            direction: space
            for direction, space in short_edge_moves.items()
            if space and not space.piece
        }
        edge_moves = short_edge_moves.values()
        # Allow a second move if the pawn has not moved.
        if not self.has_moved:
            first_moves = [
                space.get_edge(direction)
                for direction, space in short_edge_moves.items()
            ]
            edge_moves += [
                space
                for space in first_moves
                if space and not space.piece
            ]
        # Allow attacks against forward corners.
        corner_directions = [
            self.space.corner_directions[(direction_index + rotation) % 6]
            for rotation in [2, 3, 4]
        ]
        corner_moves = [
            self.space.get_corner(direction)
            for direction in corner_directions
        ]
        corner_moves = [
            space
            for space in corner_moves
            if space and space.piece and space.piece.player != self.player
        ]
        return edge_moves + corner_moves
