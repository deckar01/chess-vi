from piece import Piece


class King(Piece):
    icon = u'\u2654'

    def moves(self):
        edge_moves = self.space.edge_neighbors().values()
        corner_moves = self.space.corner_neighbors().values()
        # TODO: Allow castling.
        return [
            space
            for space in edge_moves + corner_moves
            if not space.piece or space.piece.player != self.player
        ]
