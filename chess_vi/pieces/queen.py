from piece import Piece


class Queen(Piece):
    icon = u'\u2655'

    def moves(self):
        edge_moves = self.follow_edges()
        corner_moves = self.follow_corners()
        return edge_moves + corner_moves
