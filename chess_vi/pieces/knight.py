from piece import Piece


class Knight(Piece):
    icon = u'\u2658'

    def moves(self):
        corner_moves = self.space.corner_neighbors().values()
        edge_moves = [
            corner.get_edge(corner.edge_directions[
                (corner.corner_directions.index(direction) + rotation) % 6
            ])
            for direction, corner in self.space.corner_neighbors().items()
            for rotation in [5, 0]
        ]
        return [
            space
            for space in edge_moves + corner_moves
            if space and (not space.piece or space.piece.player != self.player)
        ]
