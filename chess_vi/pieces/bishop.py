from piece import Piece


class Bishop(Piece):
    icon = u'\u2657'

    def moves(self):
        return self.follow_corners()
