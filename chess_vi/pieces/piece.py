class Piece(object):
    icon = '?'

    def __init__(self, player, space):
        self.player = player
        self.space = space
        space.piece = self
        self.captured = False
        self.has_moved = False

    def __repr__(self):
        return self.__class__.__name__ + str(self.space)

    def preview(self):
        return self.player.colorize(self.icon)

    def moves(self):
        return []

    def follow_edges(self):
        spaces = []
        for direction in self.space.edge_directions:
            spaces += self.follow_edge(direction)
        return spaces

    def follow_corners(self):
        spaces = []
        for direction in self.space.corner_directions:
            spaces += self.follow_corner(direction)
        return spaces

    def follow_edge(self, direction):
        return self.follow_direction('edges', direction)

    def follow_corner(self, direction):
        return self.follow_direction('corners', direction)

    def follow_direction(self, direction_type, direction):
        spaces = []
        space = self.space.get_direction(direction_type, direction)
        while space:
            if space.piece:
                if space.piece.player == self.player:
                    break
                spaces.append(space)
                break
            spaces.append(space)
            space = space.get_direction(direction_type, direction)
        return spaces
