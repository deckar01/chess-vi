from piece import Piece


class Rook(Piece):
    icon = u'\u2656'

    def moves(self):
        return self.follow_edges()
