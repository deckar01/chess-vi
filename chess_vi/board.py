from space import Space
from termcolor import colored


class Board(object):
    def __init__(self, size):
        self.size = size
        self.seed()
        self.grow()

    def seed(self):
        self.center = Space(0)
        self.spaces = [self.center]

    def grow(self):
        total_spaces = 3*self.size*(self.size - 1) + 1
        for space in self.spaces:
            if len(self.spaces) >= total_spaces:
                return
            for direction in space.empty_edges():
                neighbor = Space(len(self.spaces))
                space.add_edge(direction, neighbor)
                self.spaces.append(neighbor)

    def preview(self, player, moves=None):
        moves = moves or []
        all_moves = {
            move.id: []
            for piece in moves
            for move in piece['moves']
        }
        for piece in moves:
            for move in piece['moves']:
                all_moves[move.id].append(piece['piece'])
        separator = colored('|', 'white', attrs=['dark'])
        result = '\n'
        offset = '  ' * (self.size - 1)
        row = self.center.get_edge(player.edge('up-left'), self.size - 1)
        while row:
            pieces = []
            column = row
            while column:
                pieces.append(column.preview(all_moves))
                column = column.get_edge(player.edge('right'))
            result += offset
            if row.get_edge(player.edge('down-left')):
                row = row.get_edge(player.edge('down-left'))
                offset = offset[2:]
            else:
                row = row.get_edge(player.edge('down-right'))
                offset += '  '
            result += separator
            result += separator.join(pieces)
            result += separator + '\n'
        return result
