import pieces
from space import Space
from termcolor import colored


class Player(object):
    rotation = {
        'down': 0,
        'up-left': 2,
        'up-right': 4,
    }

    def __init__(self, name, color, side, board):
        self.name = name
        self.color = color
        self.side = side
        self.setup_pieces(board)
        self.captured_pieces = []

    def colorize(self, text):
        return colored(text, self.color, attrs=['bold'])

    def move(self, piece, destination):
        if destination.piece:
            self.captured_pieces.append(destination.piece)
            destination.piece.captured = True
            destination.piece.space = None
            destination.piece = None
        piece.space.piece = None
        piece.space = destination
        destination.piece = piece
        piece.has_moved = True

    def edge(self, direction):
        return self.normalize_direction('edge', direction)

    def corner(self, direction):
        return self.normalize_direction('corner', direction)

    def normalize_direction(self, direction_type, direction):
        directions = getattr(Space, direction_type + '_directions')
        direction_index = directions.index(direction)
        rotated_index = (direction_index + self.rotation[self.side]) % 6
        return directions[rotated_index]

    def setup_pieces(self, board):
        self.pieces = []
        self.home = board.center.get_corner(self.corner('down'), 4)
        king_space = self.home.get_edge(self.edge('left'), 1)
        self.king = pieces.King(self, king_space)
        self.pieces.append(self.king)
        queen_space = self.home.get_edge(self.edge('right'), 1)
        self.queen = pieces.Queen(self, queen_space)
        self.pieces.append(self.queen)
        bishop_spaces = [
            self.home.get_edge(self.edge('left'), 2),
            self.home,
            self.home.get_edge(self.edge('right'), 2),
        ]
        self.bishops = [
            pieces.Bishop(self, bishop_space)
            for bishop_space in bishop_spaces
        ]
        self.pieces += self.bishops
        knight_spaces = [
            self.home.get_edge(self.edge('left'), 3),
            self.home.get_edge(self.edge('right'), 3),
        ]
        self.knights = [
            pieces.Knight(self, knight_space)
            for knight_space in knight_spaces
        ]
        self.pieces += self.knights
        rook_spaces = [
            self.home.get_edge(self.edge('left'), 4),
            self.home.get_edge(self.edge('right'), 4),
        ]
        self.rooks = [
            pieces.Rook(self, rook_space)
            for rook_space in rook_spaces
        ]
        self.pieces += self.rooks
        pawn_home = self.home.get_corner(self.corner('up'))
        pawn_spaces = [pawn_home]
        pawn_spaces += [
            pawn_home.get_edge(self.edge(direction), x)
            for x in range(1, 5)
            for direction in ['left', 'right']
        ]
        pawn_home = pawn_home.get_edge(self.edge('down-left'))
        pawn_spaces += [
            pawn_home.get_edge(self.edge('left'), x)
            for x in range(0, 5)
        ]
        pawn_home = pawn_home.get_edge(self.edge('right'))
        pawn_spaces += [
            pawn_home.get_edge(self.edge('right'), x)
            for x in range(0, 5)
        ]
        self.pawns = [
            pieces.Pawn(self, pawn_space)
            for pawn_space in pawn_spaces
        ]
        self.pieces += self.pawns
