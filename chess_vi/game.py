from board import Board
from player import Player


class Game(object):
    player_sides = [
        'down',
        'up-left',
        'up-right',
    ]

    def __init__(self, player_names, player_colors):
        self.board = Board(9)
        self.players = {
            side: Player(name, color, side, self.board)
            for name, color, side
            in zip(player_names, player_colors, self.player_sides)
        }
        self.active_player = self.players['down']
        self.over = False

    def is_in_check(self, player):
        opponents = [
            other_player
            for other_player in self.players.values()
            if other_player != player
        ]
        enemy_moves = [
            space
            for opponent in opponents
            for moves in self.get_all_moves(opponent)
            for space in moves['moves']
        ]
        return player.king.space in enemy_moves

    def preview(self, moves=None):
        return self.board.preview(self.active_player, moves)

    def preview_all_moves(self, player):
        return self.preview(self.get_all_moves(player))

    def preview_all_players(self):
        all_moves = [
            move
            for player in self.players.values()
            for move in self.get_all_moves(player)
        ]
        return self.preview(all_moves)

    def get_all_moves(self, player):
        moves = [self.get_moves(piece) for piece in player.pieces]
        # Omit pieces without any moves.
        return [
            {'piece': move['piece'], 'moves': move['moves']}
            for move in moves
            if move['moves']
        ]

    def get_moves(self, piece):
        if piece.captured:
            return {'piece': piece, 'moves': []}
        # TODO: Prevent moving into check.
        return {'piece': piece, 'moves': piece.moves()}

    def move(self, from_id, to_id):
        from_index = int(from_id, 16)
        if from_index < 0 or from_index >= len(self.board.spaces):
            raise ValueError(
                '{} is not a valid space...'.format(from_id)
            )
        from_space = self.board.spaces[from_index]
        to_index = int(to_id, 16)
        if to_index < 0 or to_index >= len(self.board.spaces):
            raise ValueError(
                '{} is not a valid space...'.format(to_id)
            )
        to_space = self.board.spaces[to_index]
        if not from_space.piece:
            raise ValueError(
                '{} does not contain a piece...'.format(from_space)
            )
        if from_space.piece.player != self.active_player:
            raise ValueError(
                '{} is not your piece...'.format(from_space.piece)
            )
        allowed_moves = self.get_moves(from_space.piece)['moves']
        if to_space not in allowed_moves:
            raise ValueError('{} -> {} is not a valid move...'.format(
                from_space.piece,
                to_space
            ))
        self.active_player.move(from_space.piece, to_space)
        player_index = self.player_sides.index(self.active_player.side)
        next_player_index = (player_index + 1) % 3
        next_player_side = self.player_sides[next_player_index]
        self.active_player = self.players[next_player_side]
