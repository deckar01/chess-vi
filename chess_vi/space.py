from termcolor import colored


class Space(object):
    edge_directions = [
        'up-left',
        'up-right',
        'right',
        'down-right',
        'down-left',
        'left',
    ]

    corner_directions = [
        'up-left',
        'up',
        'up-right',
        'down-right',
        'down',
        'down-left',
    ]

    def __init__(self, id):
        self.id = hex(id)[2:].upper().zfill(2)
        self.piece = None
        self.edges = {
            direction: None
            for direction in self.edge_directions
        }
        self.corners = {
            direction: None
            for direction in self.corner_directions
        }

    def edge_neighbors(self):
        return {
            direction: self.edges[direction]
            for direction in self.edge_directions
            if self.edges[direction]
        }

    def corner_neighbors(self):
        return {
            direction: self.corners[direction]
            for direction in self.corner_directions
            if self.corners[direction]
        }

    def empty_edges(self):
        return [
            direction
            for direction in self.edge_directions
            if not self.edges[direction]
        ]

    def add_edge(self, direction, neighbor):
        # Connect myself to my new neighbor.
        self.edges[direction] = neighbor
        # Connect my new neighbor to myself.
        direction_index = self.edge_directions.index(direction)
        opposite_direction = self.edge_directions[(direction_index + 3) % 6]
        neighbor.edges[opposite_direction] = self
        # Connect my existing edges with the new edge neighbor.
        flank1 = self.edge_directions[(direction_index + 5) % 6]
        flank2 = self.edge_directions[(direction_index + 1) % 6]
        if self.edges[flank1] and not self.edges[flank1].edges[flank2]:
            self.edges[flank1].add_edge(flank2, neighbor)
        if self.edges[flank2] and not self.edges[flank2].edges[flank1]:
            self.edges[flank2].add_edge(flank1, neighbor)
        # Connect my existing edges with the new corner neighbor.
        rear1 = self.edge_directions[(direction_index + 4) % 6]
        rear2 = self.edge_directions[(direction_index + 2) % 6]
        corner1 = self.corner_directions[(direction_index + 1) % 6]
        corner2 = self.corner_directions[(direction_index + 0) % 6]
        if self.edges[rear1] and not self.edges[rear1].corners[corner1]:
            self.edges[rear1].add_corner(corner1, neighbor)
        if self.edges[rear2] and not self.edges[rear2].corners[corner2]:
            self.edges[rear2].add_corner(corner2, neighbor)

    def add_corner(self, direction, neighbor):
        # Connect myself to my new neighbor.
        self.corners[direction] = neighbor
        # Connect my new neighbor to myself.
        direction_index = self.corner_directions.index(direction)
        opposite_direction = self.corner_directions[(direction_index + 3) % 6]
        neighbor.corners[opposite_direction] = self

    def get_edge(self, direction, distance=1):
        return self.get_direction('edges', direction, distance)

    def get_corner(self, direction, distance=1):
        return self.get_direction('corners', direction, distance)

    def get_direction(self, direction_type, direction, distance=1):
        if distance == 0:
            return self
        space = getattr(self, direction_type)[direction]
        if not space:
            return None
        return space.get_direction(direction_type, direction, distance - 1)

    def __repr__(self):
        return '<{}>'.format(self.id)

    def preview(self, moves):
        if self.piece:
            icon = self.piece.preview()
        else:
            icon = ' '
        if self.id not in moves:
            return (
                colored(self.id[0], 'grey') +
                icon +
                colored(self.id[1], 'grey')
            )
        colors = ['on_' + piece.player.color for piece in moves[self.id]]
        if len(set(colors)) > 2:
            colors = list(set(colors))
        if len(set(colors)) == 2:
            unique_colors = list(set(colors))
            count1 = len([
                color for color in colors
                if color == unique_colors[0]
            ])
            count2 = len([
                color for color in colors
                if color == unique_colors[1]
            ])
            if count1 == count2:
                colors = [unique_colors[0], 'on_grey', unique_colors[1]]
            if count1 > count2:
                colors = [unique_colors[0], unique_colors[1], unique_colors[0]]
            if count1 < count2:
                colors = [unique_colors[1], unique_colors[0], unique_colors[1]]
        if len(colors) == 1:
            colors = ['on_grey', colors[0], 'on_grey']
        if len(colors) == 2:
            colors = [colors[0], 'on_grey', colors[0]]
        return (
            colored(self.id[0], 'grey', colors[0 % len(colors)]) +
            colored(icon, 'grey', colors[1 % len(colors)]) +
            colored(self.id[1], 'grey', colors[2 % len(colors)])
        )
