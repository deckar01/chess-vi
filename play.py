from chess_vi import Game
from termcolor import colored


def show_help():
    print 'SHOW ' + colored('[ME|ALL]', 'white', attrs=['dark'])
    print '    Shows the board.'
    print '    The ME flag shows your available moves.'
    print '    The ALL flag shows all players\' moves .'
    print colored('    example: SHOW', 'white', attrs=['dark'])
    print colored('    example: SHOW ME', 'white', attrs=['dark'])
    print colored('    example: SHOW ALL', 'white', attrs=['dark'])


def move_help():
    print colored('(MOVE)', 'white', attrs=['dark']) + ' FROM TO'
    print '    Moves your piece from one space to another.'
    print colored('    example: 73 4f', 'white', attrs=['dark'])
    print colored('    example: MOVE 73 4f', 'white', attrs=['dark'])


game = Game(['Harry', 'Moe', 'Curly'], ['red', 'blue', 'yellow'])

while not game.over:
    print game.preview()
    hint = 'Type "HELP" for options.'
    print colored(hint, 'white', attrs=['dark'])
    while True:
        try:
            command = raw_input(
                game.active_player.colorize(game.active_player.name) +
                ': '
            )
            command = command.upper().split(' ')
            if command[0] == 'HELP':
                print 'HELP:'
                show_help()
                move_help()
            elif command[0] == 'SHOW':
                if len(command) > 1 and command[1] == 'ALL':
                    print game.preview_all_players()
                elif len(command) > 1 and command[1] == 'ME':
                    print game.preview_all_moves(game.active_player)
                else:
                    print game.preview()
            elif command[0] == 'MOVE':
                if len(command) >= 3:
                    game.move(command[1], command[2])
                    break
                else:
                    print colored('Invalid move.', 'grey', 'on_red', attrs=['dark'])
                    move_help()
            else:
                if len(command) >= 2:
                    game.move(command[0], command[1])
                    break
                else:
                    print colored('Invalid move.', 'grey', 'on_red', attrs=['dark'])
                    move_help()
        except ValueError as e:
            print colored(e.message, 'grey', 'on_red', attrs=['dark'])
        except KeyboardInterrupt as e:
            print '\nThanks for playing...'
            exit(0)
